import { Component } from '@angular/core';
import { Platform, ViewController, NavController } from 'ionic-angular';
import { IonicPage } from 'ionic-angular';
import { BasePage } from '../../core/base-page';
import { Config } from '../../config';
import { Utils } from '../../core/providers/utils';
import { Api } from '../../providers';

@IonicPage()
@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage extends BasePage {

	name: string = 'HomePage';

	gradingTestModel: HKIaidoApiModel.GradingTestParam = {
		name_zh: "",
		name_en: "",
		gender: "",
		tel: "",
		email: "",
		grading_level: "",
		grading_date: "",
	};

	memberList: HKIaidoApiModel.Memeber[] = [];
	
	constructor(
		protected platform: Platform,
		protected view: ViewController,
		protected nav: NavController,
		protected utils: Utils,
		protected api: Api
	) {
		super(platform, view, nav, utils);
		Config.DEBUG_VERBOSE && console.log('HomePage constructor');
	}

	ionViewDidEnter () {
		this.init ();
	}

	//	=====================
	//	|		Init		|
	//	=====================
	//#region Init Methods
	init () {
		setTimeout (() => {
			this.getMemeberList ();
		}, 500);
	}
	//#endregion

	//	=====================
	//	|	UI Callbacks	|
	//	=====================
	//#region UI Callbacks
	onSubmitClicked () {
		console.log (this.gradingTestModel);
		this.getMemeberList ();
	}
	//#endregion

	//	=========================
	//	|		API Calls		|
	//	=========================
	//#region API Calls
	getMemeberList () {
		this.api.getMemebers ().then ((resp: HKIaidoApiModel.MemeberListResult) => {
			if (!resp.err_code) {
				this.memberList = resp.memebers;
				console.log (this.memberList);
			}
		});
	}
	//#endregion
}