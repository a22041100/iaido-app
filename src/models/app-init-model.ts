namespace HKIaidoApiModel {
    /** API Model: POST /init API parameters */
    export class InitParam {
        platform: string;
        device_id: string;
        version: string;
    }

    /** API Model: POST /init API Result Model */
    export class InitResult extends BaseResposneModel {
        token?: string;
        lastest_version: string;
        force_update: boolean;
        new_versions: AppNewVersions[];
        app_config: AppConfig[];
    }
    
    /** App new versions model */
    export class AppNewVersions {
        id: number;
        platform: string;
        code: string;
        release_notes: string;
        force_update: string;
        publish_date: string;
        status: string;
    }

    /** App config model */
    export class AppConfig {
        id: number;
        website_url: string;
        android_app_url: string;
        ios_app_url: string;
    }
}