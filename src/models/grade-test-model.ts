namespace HKIaidoApiModel {
    export class GradingTestParam {
        name_zh?: string;
        name_en?: string;
        gender: string;
        tel: string;
        email: string;
        grading_level: string;
        grading_date: string;
    }
}