namespace HKIaidoApiModel {
    export class Memeber {
        id: string;
        joindate: string;
        name_zh: string;
        name_en: string;
        age: string;
        nationality: string;
        hkid: string;
        dob: string;
        job: string;
        tel: string;
        address: string;
        office_tel: string;
        mobile_tel: string;
        gender: string;
        email: string;
        urgent_name: string;
        urgent_tel: string;
        urgent_relationship: string;
        height: string;
        grade: string;
        last_failed_date: string;
        grade_status: string;
    }

    export class MemeberListResult extends BaseResposneModel {
        memebers: Memeber[];
    }
}