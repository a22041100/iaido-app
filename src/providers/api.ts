import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Utils } from '../core/providers/utils';
import { BaseService } from '../core/base-service';

@Injectable()
export class Api extends BaseService {
	
	// override API URL prefix and anonymous API key
	//protected api_prefix: string = 'http://localhost/juicylauncher2_web/api';
	protected api_prefix: string = 'http://127.0.0.1/iaido-cms/api';
	
	constructor(platform: Platform, utils: Utils) {
		super(platform, utils);
	}

	/** Set API header jwt token */
	public setJwtHeader (jwtToken: string) {
		this.headers.append ('Authorization', jwtToken);
	}

	/** API: Post init to activate this device */
	public postInit (postParam: HKIaidoApiModel.InitParam): Promise<HKIaidoApiModel.InitResult> {
		return this.post ('/init', postParam);
	}

	/** API: Get member list from server database */
	public getMemebers (): Promise<HKIaidoApiModel.MemeberListResult> {
		return this.getRemote ('/memeber');
	}
}