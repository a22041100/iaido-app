/**
 * Base class for application, with common workflow when upon app start
 * @Component to be defined in child classes
 **/
import { ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { Config } from '../config';
import { Utils } from './providers/utils';
import { BasePage } from './base-page';
import { Api } from '../providers';
import { NewVersionPage } from './components/new-version/new-version';

export class BaseApp {
	
	// the root nav is a child of the root app component
	// @ViewChild(Nav) gets a reference to the app's root nav
	@ViewChild(Nav) nav: Nav;

	// default page to display
	public rootPage: any;

	constructor(
		protected platform: Platform,
		protected api: Api,
		protected utils: Utils
	) {
		Config.DEBUG_VERBOSE && console.log('BaseApp constructor');
		
		// override Android hardware back button
		platform.registerBackButtonAction(() => {
			
			// check current page name
			let page = <BasePage>this.nav.root;
			if (!this.nav.canGoBack() && page.name == Config.ROOT_PAGE_NAME) {
				// show Exit App confirmation box
				this.utils.showConfirm('', this.utils.instantLang('MSG.CONFIRM_EXIT_APP'), () => {
					this.platform.exitApp();
				});
			} else {
				// normal go back navigation
				this.nav.pop();
			}
		});
		
		platform.ready().then(() => {
			// Okay, so the platform is ready and our plugins are available.
			// Here you can do any higher level native things you might need.
			this.utils.setupStatusbar();
			
			// JuicyLauncher setup
			utils.setupLang();
			utils.setupGoogleAnalytics();
			// utils.setupOneSignal();
			
			// check force or soft update
			// this.checkVersion();
			
			this.onAppLoaded();
		});
	}

	// Version checking	
	protected checkVersion() {

		// this.api.getVersions().then(data => {
		// 	// Prepare modal or popover, based on whether need to force upgrade
		// 	if (data) {
		// 		if (data.force_upgrade) {
		// 			this.utils.showModal(NewVersionPage, { version_response: data });
		// 		} else {
		// 			// check whether user has dismissed version upgrade notice before
		// 			let latest_version_code: string = data.latest_version;
		// 			let key: string = 'VERSION_CHECK_FROM_' + data.curr_version + '_TO_' + latest_version_code;
		// 			this.utils.getLocal(key, false).then(skipped => {
		// 				if (!skipped) {
		// 					this.utils.showModal(NewVersionPage, { version_response: data });
		// 				}
		// 			});
		// 		}
		// 	}
			
		// 	// indicate the app is successfully loaded
		// 	this.onAppLoaded();
		// }).catch(err => {
		// 	// version cannot be found from server, but still proceed to init the app
		// 	this.onAppLoaded();
		// });

		this.utils.currentVersion ().then ((curVersion: string) => {
			let appInitParam: HKIaidoApiModel.InitParam = {
				platform: 'android',
				device_id: 'abc',
				version: curVersion,
			};

			this.api.postInit (appInitParam).then ((resp: HKIaidoApiModel.InitResult) => {
				if (!resp.err_code) {
					if (resp.token) {
						this.utils.setLocal (Config.SAVED_JWT_TOKEN_KEY, resp.token);
						this.api.setJwtHeader (resp.token);
						this.onAppLoaded();
					} else {
						this.utils.getLocal (Config.SAVED_JWT_TOKEN_KEY, null).then ((token: string) => {
							if (token) {
								this.api.setJwtHeader (token);
								this.onAppLoaded();
							} else {
								this.onAppLoaded();
							}
						});
					}
				} else {
					this.onAppLoaded();
				}
			});
		});
	}
	
	// inherit this function from child class (e.g. MyApp)
	protected onAppLoaded() {
		Config.DEBUG_VERBOSE && console.log('BaseApp onAppLoaded');
	}

	// [For App with Tab / Sidemenu root only]
	protected openPage(page: any) {
		// Reset the content nav to have just this page
		// we wouldn't want the back button to show in this scenario
		if (this.nav && page) {
			this.nav.setRoot(page);
		}
	}
}
