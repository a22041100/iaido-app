import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { BaseApp } from '../core/base-app';
import { Config } from '../config';
import { Utils } from '../core/providers/utils';
import { Api } from '../providers';

@Component({
	templateUrl: 'app.html'
})
export class MyApp extends BaseApp {
	
	constructor(platform: Platform, api: Api, utils: Utils) {
		super(platform, api, utils);
		Config.DEBUG_VERBOSE && console.log('MyApp constructor');
	}

	// override parent
	protected onAppLoaded() {
		// set rootpage only when the app is ready
		Config.DEBUG_VERBOSE && console.log('MyApp onAppLoaded');
		this.rootPage = Config.START_PAGE;

		// check local jwt token
		this.utils.getLocal (Config.SAVED_JWT_TOKEN_KEY, 'undefined').then ((token: string) => {
			if (token !== 'undefined') {
				console.log ("found saved jwt token, going to set to the header: " + token);
				this.api.setJwtHeader (token);
			} else {
				console.log ("can not find saved jwt token, going to call init api");
				this.initApi ();
			}
		});
	}

	private initApi () {
		this.utils.currentVersion (). then ((curVersion: string) => {
			let initParam: HKIaidoApiModel.InitParam = {
				platform: 'android',
				device_id: 'abcdefg',
				version: curVersion,
			}

			this.api.postInit (initParam).then ((resp: HKIaidoApiModel.InitResult) => {
				if (resp.token) {
					this.utils.setLocal (Config.SAVED_JWT_TOKEN_KEY, resp.token);
					this.api.setJwtHeader (resp.token);
				}
			});
		});
	}
}